<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('request_id')->unsigned();
            $table->integer('driver_id')->unsigned();
            $table->string('driver_lat');
            $table->string('driver_lang');
            $table->string('destination_driver');
            $table->string('destination_user');
            $table->string('start_time');
            $table->string('end_time');
            $table->string('price');
            $table->string('status');
            $table->timestamps();
        });

        Schema::table('trips', function (Blueprint $table) {  
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('request_id')->references('id')->on('requests');
            $table->foreign('driver_id')->references('id')->on('drivers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
