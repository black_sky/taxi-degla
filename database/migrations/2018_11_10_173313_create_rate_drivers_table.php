<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rate_drivers', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('driver_id')->unsigned();
                $table->string('rate');
                $table->string('comment');
                $table->timestamps();
            });

            Schema::table('rate_drivers', function (Blueprint $table) {  
                $table->foreign('driver_id')->references('id')->on('drivers');
            });
     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rate_drivers');
    }
}
