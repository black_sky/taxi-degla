<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class createDatabases extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this command will help you to create new command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::statement('CREATE DATABASE IF NOT EXISTS'." ".env('APP_NAME')." ".
        'CHARACTER SET utf8mb4
        COLLATE utf8mb4_unicode_ci');

    }
}
